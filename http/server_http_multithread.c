#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>



char str[100];
const char* url_name;
void* thread_client(void* socket_client){
    size_t nread;
    char buffer[4096];
    char buffer_file[4096];
    char * buffer_response_method;
    char * buffer_Response_page;
    char * buffer_response_protocol;
    char simple_header_success[1096];
    strcpy(simple_header_success, "HTTP/1.1 200 OK\n\n");
    printf("%s\n", simple_header_success);

    char simple_header_error[1096];
    strcpy(simple_header_error, "HTTP/1.1 404 Notfound\n\n");
    printf("%s\n", simple_header_error);
    //while(1)
    //{
        bzero( str, 100);

        read(*(int*)socket_client,str,100);
        //printf("buffer %s\n", str);
 
        //Parse the request
        buffer_response_method = strtok (str," \t");
        printf("Method : %s\n", buffer_response_method);

        buffer_Response_page = strtok (NULL, " \t");
        buffer_Response_page++;
        url_name = buffer_Response_page;
        if (strcmp(url_name, "/") == 0) url_name = "index.html";
        printf("Url de la page : %s\n", url_name);

        buffer_response_protocol = strtok (NULL, " \r\n");
        printf("Protocol : %s\n", buffer_response_protocol);

        FILE * file = fopen(url_name, "r");
        
        if (file) {
            printf("send header 200 : %s\n", simple_header_success);
            write(*(int*)socket_client, simple_header_success, strlen(simple_header_success));
            while ((nread = fread(buffer_file, 1, 4096, file)) > 0){
                //printf("%s buffer_file : ", buffer_file);
                write(*(int*)socket_client, buffer_file, nread);
            }

        } else {
            printf("send header 404 : %s\n", simple_header_error);
            write(*(int*)socket_client, simple_header_error, strlen(simple_header_error));
        }
        fclose(file);
        shutdown(*(int*)socket_client, SHUT_RDWR);
        close(*(int*)socket_client);
        pthread_exit(NULL);
    //}
}

int main()
{
    pthread_t thread_client_t;
    int listen_fd, conn_fd;

    struct sockaddr_in servaddr;
    struct sockaddr cliaddr;
    socklen_t sa_len=sizeof(cliaddr);

    listen_fd = socket(AF_INET, SOCK_STREAM, 0);

    bzero( &servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htons(INADDR_ANY);
    servaddr.sin_port = htons(8080);

    if(bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr)) == -1){
        printf("Bind failed");
    }

    listen(listen_fd, 10);


    //conn_fd = accept(listen_fd, (struct sockaddr*) NULL, NULL);
    while(1){
        conn_fd = accept(listen_fd, &cliaddr, &sa_len);
        pthread_create (&thread_client_t, NULL, thread_client, &conn_fd);
    }


}