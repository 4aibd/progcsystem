// Server side C/C++ program to demonstrate Socket programming 
#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#define PORT 8080 
#define BUF_SIZE 1024
#define MAX_CLIENTS 10
typedef struct sockaddr_in SOCKADDR_IN;
typedef int SOCKET;

typedef struct
{
   SOCKET sock;
   char name[BUF_SIZE];
}Client;

int main(int argc, char const *argv[]) 
{    
    int socket_fd = 0, accept_fd = 0, request, new_socket;

        // MULTI CLIENTS
    #define max(x,y) ((x)>(y)?(x):(y))
    fd_set fds;
    int fds_max;
    int connections_actives;
    int connections_actives_count = 0;
    int connections[10];
    int clientSize = 0;

    //-------------
    char buffer[BUF_SIZE];                                // Tampon pour stocker les données
    int         socketListener  = InitConnection();          // Descripteur du socket qui écoute sur le port
    int         currentIndex = 0;                            // Index pour parcourir les clients
    int         maxFd = socketListener;                      // Stock la plus grande valeur de File Descriptor
    Client      client[MAX_CLIENTS];                        // Variable contenant les données client
    fd_set      readfds;

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    // BEGIN LISTEN
    listen(socket_fd, 10);
    printf("_SERVER - start : server launched, waiting for client\n");

    while(1)
    {
        FD_ZERO(&fds);
        SOCKADDR_IN sin = { 0 };
        if(connections_actives_count > 0)
            FD_SET(connections_actives, &fds);

        for( int i_connections = 0; i_connections < sizeof(connections); i_connections++ )
        {
            FD_SET(connections[i_connections], &fds);
        }

        select(fds_max+1, &fds, NULL, NULL, NULL);


        for( int i_connections = 0; i_connections < sizeof(connections); i_connections++ )
        {
            if(FD_ISSET(connections[i_connections], &fds))
            {
                read(connections[i_connections],&sin, buffer);
                // TODO : if 0 disconnect
            }

            if(FD_ISSET(connections[i_connections], &fds))
            {
                clientSize = sizeof(client);
                new_socket = accept(socket_fd, (struct sockaddr *)&client, (socklen_t*)&clientSize);
                connections[i_connections] = new_socket;
                i_connections++;
            }
        }
    }
} 

