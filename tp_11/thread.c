#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <sys/time.h> 
#include <sys/select.h> 
#include <pthread.h>
#include <sys/types.h>

void* write_line(void * car)
{
    char * new_car = (char*) car;
    printf("_thread, %s \n", new_car);
}

int error(int first_err)
{
    if (first_err)
    {
        printf("An error occured: %d", first_err);
        return 1;
    }
}
    


int main(int argc,char** argv) 
{
    pthread_t first_thread;
    pthread_t second_thread;
    int first_err,second_err;

    printf("Create first thread \n");
    first_err = pthread_create(&first_thread, NULL,write_line, (void*)"test d ecriture");

    printf("Create second thread \n");
    second_err = pthread_create(&second_thread, NULL, write_line, (void*)"test d ecriture");

    error(first_err);
    error(second_err);
    
    printf("Waiting for the thread to end...\n");

    pthread_join(first_thread, NULL);
    pthread_join(second_thread, NULL);
    
    printf("\n Thread ended.\n");    

    return 0;
}

// gcc -lpthread main.c -o main