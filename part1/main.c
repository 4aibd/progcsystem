#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

struct Etudiant
{
    char nom[25];
    int age;
    int id_ecole;
}Etudiant;

int main()
{
    char c1[5];

    //Ecrire dans la chain de caractère
    fgets(c1,sizeof(char)*5,stdin);
    
    //Affichage
    printf("affiche : %s \n",c1);
    
    //Ouvre un fichier et donne le droit d'ecrire
    FILE* p = fopen("tmp.txt", "w+");

    //Ecrire dans le fichier
    fputs(c1,p); 
    
    // Fermeture du fichier
    fclose(p);

    // Partie 2
    FILE* etu = fopen("tmpEtudiant.txt","w+");

    // Création de la structure
    struct Etudiant etudiant1;

    // Assignation des valeurs de l'etudiant
    strcpy(etudiant1.nom,"Olivier") ;
    etudiant1.age = 50 ;
    etudiant1.id_ecole = 1;

    // Ecriture dans le fichier
    fwrite(&etudiant1,sizeof(Etudiant),1,etu);

    fclose(etu);

    // File en mode read
    FILE* eturead = fopen("tmpEtudiant.txt","r+");

    // Création de la structure
    struct Etudiant etudiant2;
    
    //lecture du fichier -> Struct
    fread(&etudiant2,sizeof(Etudiant),1,eturead);

    // Affichage de la data
    printf("nom : %s, age : %d, id_ecole : %d \n",etudiant2.nom,etudiant2.age,etudiant2.id_ecole);

    fclose(eturead);
    
    // TP 4
    FILE * etuSom =  fopen("SomEtu.txt","w+");

    struct Etudiant etu1;
    struct Etudiant etu2;
    struct Etudiant etu3;

    fprintf(etuSom,"%s %d %d %s","Olivier",50,1,"\n");
    fprintf(etuSom,"%s %d %d %s","Manitra",24,1,"\n");
    fprintf(etuSom,"%s %d %d %s","Hugo",6,1,"\n");

    fclose(etuSom);

    FILE * etuSomRead =  fopen("SomEtu.txt","r+");

    fscanf(etuSomRead,"%s %d %d",etu1.nom,&etu1.age,&etu1.id_ecole);
    fscanf(etuSomRead,"%s %d %d",etu2.nom,&etu2.age,&etu2.id_ecole);
    fscanf(etuSomRead,"%s %d %d",etu3.nom,&etu3.age,&etu3.id_ecole);

    fclose(etuSomRead);

    printf("nom : %s, age : %d, id_ecole : %d \n",etu1.nom,etu1.age,etu1.id_ecole);
    printf("nom : %s, age : %d, id_ecole : %d \n",etu2.nom,etu2.age,etu2.id_ecole);
    printf("nom : %s, age : %d, id_ecole : %d \n",etu3.nom,etu3.age,etu3.id_ecole);
}