#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    int i = 10;
    int pid;
    
    pid = fork();
    if (pid == -1 )
	{	fprintf(stderr,"le fork est en echec\n");
		exit(1) ;
	}
	
    if(pid!=0)
    {  /* on est dans le processus pere*/
		printf("PERE: valeur de fork    :  %4d\n",pid);
     	printf("PERE: mon pid est   : %4d\n",getpid());
     	printf("PERE: pour moi i = %2d\n",i);
        printf("	au revoir, j'etais le processus de pid = %4d\n",getpid());
	}

    if(pid==0)
	{  /* on est dans le processus fils*/
	  	printf("	FILS: dans FILS, valeur de fork	: %4d\n",pid);
     	printf("	FILS: mon pid est   : %4d\n",getpid());
	    printf("	FILS: le pid de mon PERE est    : %4d\n",getppid());
        i = 25;
        printf("	FILS: pour moi i = %2d \n",i);
        printf("	au revoir, j'etais le processus de pid = %4d\n",getpid());
	}
	printf("PEREFILS: fin du processus en cours de no de pid    :  %4d\n",getpid());
    return 0;
}