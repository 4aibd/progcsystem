#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <time.h>

int fonction_fils(int i)
{
    int status;

    printf("FILS >> je suis le fils numéro %d, de PID=%d\n", i, getpid());
    srand(time(NULL) + getpid());
    status = rand() % 30;
    printf("FILS >> Nombre aléatoire généré\n");

    exit(status);
}

int main(int argc, char const *argv[])
{
    pid_t n, fils;
    int i, status;

    for (int i = 1; i <= 3; i++)
    {
        n = fork();
        if (n == 0)
        {
            fonction_fils(i);
        }
    }
    while ((fils = wait(&status)) > 0)
    {
        if (WIFEXITED(status))
        {
            printf("PERE >> fin normale du fils\n");
            printf("PERE >> fin du fils de PID:%d - Valeur retour : %d\n", fils, WEXITSTATUS(status));
        }
        else
        {
            if (WIFSIGNALED(status))
            {
                printf("PERE >> FIN ANORMAL\n");
                printf("PERE >> SIGNAL RESPONSABLE : %d", WTERMSIG(status));
            }
        }
    }
}