#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <time.h>

void run_bash_function()
{
    int status;
    printf("    FILS >> je suis le fils avec pour PID : %d\n",getpid());
    //execl("/bin/ls","ls","-al","/",NULL);
    char *argv[]={"ls","-al","/users/manitraranaivoharison",NULL};
    execv("/bin/ls",argv);
 
}

int main(int argc, char const *argv[])
{
    pid_t n, fils;
    int i, status;

    n = fork();
    if (n == 0)
    {
        run_bash_function();
    }
    
    while ((fils = wait(&status)) > 0)
    {
        if (WIFEXITED(status))
        {
            printf("PERE >> fin normale du fils\n");
            printf("PERE >> fin du fils de PID:%d - Valeur retour : %d\n", fils, WEXITSTATUS(status));
        }
        else
        {
            if (WIFSIGNALED(status))
            {
                printf("PERE >> FIN ANORMAL\n");
                printf("PERE >> SIGNAL RESPONSABLE : %d", WTERMSIG(status));
            }
        }
    }
}