#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int ram_numb(int a,int b){
    return (rand()%(b-a) + a);
}

int main(int argc,char** argv)
{

    pid_t n;
    pid_t nf;
    nf=getpid();

    for(int i = 0 ; i < 3 ; i++){
        if(getpid()==nf){
            n = fork();
            printf("    FILS Je suis le fils, mon PID est   :   %4d \n",n);
            int x = rand_numb(0,10);
            printf("variable aleatoire x: %d", x);
        }
    }
    
    if(n>0)
    {   
        printf("je suis le PERE, pid: %4d et mon fils est   :   %4d\n",getpid(),n);
    }

    if(n==-1)
	{	fprintf(stderr,"le fork est en echec\n");
		exit(1);
	}

    printf("PEREFILS: fin du processus en cours de no de pid    :  %4d\n",getpid());

    return 0;
}