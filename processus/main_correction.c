#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

int main(int argc,char** argv)
{
    int i = 10;
    pid_t n;
    n=fork();

    if(n==0)
    {
        printf("    je suis dans le FILS, pid=%4d, et mon pere est : %4d \n",getpid(),getppid());
        i = 25;
        printf("    Pour moi, i=%d \n",i);

    }
    else
        if(n>0)
        {
            printf("je suis le PERE, pid: %4d et mon fils est:   %4d\n",getpid(),n);
            printf("Pour moi, i=%d \n",i);
        }

    else
	{	fprintf(stderr,"le fork est en echec\n");
		exit(1);
	}
    printf("PEREFILS: fin du processus en cours de no de pid    :  %4d\n",getpid());

    return 0;
}