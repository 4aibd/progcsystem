#include<stdio.h>
#include<string.h>      //strlen
#include<stdlib.h>      //strlen
#include<sys/socket.h>
#include<arpa/inet.h>   //inet_addr
#include<unistd.h>      //write
#include<pthread.h>     //for threading, link with lpthread
 #define PORT 8080 

//  gcc server.c -lpthread -o server

void *connection_handler(void *);
 
int main(int argc , char *argv[])
{
    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
     
    //  Cree un socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("le socket n'a pas pu être créé");
    }
    puts("\nSocket créé");
     
    //  Prepare la structure sockaddr_in
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( PORT );
     
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("bind à échoué, erreur");
        return 1;
    }
    puts("\nbind fait");
        //  En ecoute
    listen(socket_desc , 10);
     
    //  Accepte les connexion entrante
    puts("\nEn attente de connection entrant...");
    c = sizeof(struct sockaddr_in);
     
	pthread_t thread_id;

    while((client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)))
    {   
        puts("\nConnection accepté\n");
        printf("client socket: %d\n", client_sock);
         
        if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &client_sock) < 0)
        {
            perror("\nle socket n'a pas pu être créé");
            return 1;
        }
        pthread_detach( thread_id);
    }
     
    if (client_sock < 0)
    {
        perror("\nerreur d accès");
        return 1;
    }
     
    return 0;
}
 
/*
 * connection_handler : procedure qui gérera la connexion pour chaque client
 * */
void *connection_handler(void *socket_desc)
{
    //Get the socket descriptor
    int sock = *(int*)socket_desc;
    int read_size;
    char *message , client_message[2000];
     
    //Send some messages to the client
    message = "Salutations! Je suis votre gestionnaire de connexion\n";
    write(sock , message , strlen(message));

    //  Recevoir un message du client
    while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
    {   
        //  marqueur de fin de chaîne
		client_message[read_size] = '\0';

        //  affiche le message du client
        printf(client_message);
		
		//  Renvoyer le message au client
        write(sock , client_message , strlen(client_message));
		
		//  effacer le tampon de message
		memset(client_message, 0, 2000);

    }
    close(sock);
     
    if(read_size == 0)
    {
        puts("\nclient deconnecté");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("\nrecv echoué");
    }
         
    return 0;
} 