#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <sys/time.h> 
#include <sys/select.h> 
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

// gcc -lpthread main.c -o main

void* write_line(void * car)
{      
    char * new_car = (char*) car;
    printf("_thread, %s \n", new_car);

    /* Debut de la zone protegee. */
    pthread_mutex_lock(&mutex);

    sleep(5);

    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex);

}

void* printer(void * car)
{
    char * new_car = (char*) car;
    printf("_thread, %s \n", new_car);

    pthread_cond_wait (&cond, &mutex);
    printf("le compteur est terminé");
}


int error(int erreur)
{
    if (erreur)
    {
        printf("Une erreur s'est produite: %d", erreur);
        return 1;
    }
}
    


int main(int argc,char** argv) 
{
    pthread_t first_thread;
    pthread_t second_thread;
    int first_err,second_err;
    

    printf("Créé le premier thread \n");
    first_err = pthread_create(&first_thread, NULL,write_line, (void*)"test d ecriture");
   

    printf("Créé le deuxieme thread \n");
    second_err = pthread_create(&second_thread, NULL, printer, (void*)"test d ecriture");

    error(first_err);
    error(second_err);
    
    printf("Attend la fin des threads...\n");

    pthread_join(first_thread, NULL);
    pthread_join(second_thread, NULL);
    
    printf("\n Threads terminés.\n");    

    return 0;
}

