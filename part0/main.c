#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main() {
    char T;
    T = 'h';

    //Write
    int p = open("tmp.txt", O_WRONLY | O_CREAT, 0666);

    //printf("%d \n",p) ;

    int result = (int) write(p, &T, 1);

    //printf("%d \n",result);
    close(p);

    //Read
    int p2 = open("tmp.txt", O_RDONLY | O_CREAT, 0666);

    char c;
    read(p2, &c ,1);

    printf("%c \n",c);
    close(p2);

    // Tableau
    char* tab;

    tab = "Hello World";
    int len = strlen(tab);

    // Ecriture
    printf("len of tab : %d \n",len);
    int p3 = open("tmp3.txt", O_WRONLY | O_APPEND | O_CREAT, 0666);
    for(int i = 0 ; i< len ; i++)
    {   
        char tmp = tab[i];
        
        //printf("loop : %c \n",tmp);
        write(p3, &tmp, 1);
    }
    close(p3);

    // LECTURE
    char tabC[12];
    tabC[11] = '\n';

    int p4 = open("tmp3.txt", O_RDONLY | O_CREAT, 0666);

    for(int i = 0 ; i< 11 ; i++)
    {   
        char tmp;
        
        //printf("loop : %c \n",tmp);
        read(p4, &tmp ,1);
        tabC[i] = tmp;
    }
    close(p4);

    printf("tabC : %s \n",tabC);

    // LECTURE
    char tabC2[12];
    tabC2[11] = '\n';

    int p5 = open("tmp3.txt", O_RDONLY | O_CREAT, 0666);

    lseek(p5,-1,SEEK_END);

    for(int i = 0 ; i< 11 ; i++)
    {   
        char tmp;
        
        //printf("loop : %c \n",tmp);
        read(p5, &tmp ,1);
        tabC2[i] = tmp;
        lseek(p5, -2,SEEK_CUR);
        printf("loop : %c \n",tmp);

    }
    close(p5);

    printf("\n tabC2 : %s \n",tabC2);

    return 0;
}