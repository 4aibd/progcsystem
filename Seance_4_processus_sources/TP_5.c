#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char** argv)
{
	pid_t n;
	int i=5;
	printf("Bonjour\n");
	printf("\033[37mJe suis le processus n°%d, i=%d\n",getpid(),i); // père 
	n=fork();
	if (n==0)
	{
		printf("\033[31mFils >> Je suis le fils : pid=%d, mon père est %d\n",getpid(), getppid());
		i=25;
		printf("\033[31mFils >> pour moi, i=%d\n",i);
	}
	else
		if (n>0)
	{
		printf("\033[34mPère >> Je suis le père : pid=%d, mon fils est %d\n",getpid(), n);
		printf("\033[34mPère >> pour moi, i=%d\n",i);
	}
	else
	{
		printf("Erreur fork\n");
		exit(-1);
	}
	printf("\033[0mAu revoir, j'étais le processus de pid=%d\n",getpid());
	return 0;
}
