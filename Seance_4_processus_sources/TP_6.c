#include <stdlib.h>  // rand(), srand()
#include <time.h>    // time()
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int fonction_fils(int i)
{
	int status;

	printf("Fils >> ### Nouveau fils ###\n");
	printf("Fils >> Je suis le fils numéro %d de PID : %d\n",i,getpid());
	printf("Fils n°%d >> Génération du nombre aléatoire \n",i);
	srand(time(NULL)+getpid());
	status=rand()%30;

	exit(status);
}

int main(int argc, char** argv)
{
	pid_t n=1,fils;
	int i=1,status;
	//printf("### Début du père ###\n");

	for(i=0;i<3;i++)
	{
		n=fork();
		if(n==0) fonction_fils(i);
	}
	while ((fils=wait(&status))>0)
	{
		if (WIFEXITED(status))
		{
			printf("Père >> ### Fin du fils normale ###\n");
			printf("Père >> Fin du fils de PID : %d - Valeur retour : %d \n",fils,WEXITSTATUS(status));
		}
		else
			if(WIFSIGNALED(status))
			{
				printf("Père >> Terminaison du fils à cause d'un signal\n");
				printf("Père >> Signal responsable : %d\n",WTERMSIG(status));
			}
	}

	printf("### FIN du \"MAXI\"-père ###\n");
	return 0;

}
