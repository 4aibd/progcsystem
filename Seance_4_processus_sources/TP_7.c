#include <stdlib.h>  // rand(), srand()
#include <time.h>    // time()
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

//Mini shell





int main(int argc, char** argv)
{
	pid_t forked_shell;
	int i=0,status;
	char chaine[3][30];
	printf("Ouverture du mini shell\n");

	while (1)
	{
	printf("User@laptop$ ");

	fscanf(stdin,"%s %s %s",chaine[0],chaine[1],chaine[2]);

	
	forked_shell=fork();

	if(forked_shell==0)
	{
		//On peut execlp
		execlp(chaine[0],chaine[0],chaine[1],chaine[2],NULL);
		printf("Exec failed");

	}
	else
	{
		wait(&status);
	}
	}
	return 0;
}
