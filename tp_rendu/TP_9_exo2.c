#include <stdlib.h> 
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

//Mini shell

static volatile int keepRunning=1;

void handle_signal(int signal) {
    const char *signal_name;

    switch (signal) {

        case SIGINT:
            signal_name = "SIGINT";
            printf("tester CTRL+Z\n");
            break;

        case SIGTSTP:
            signal_name = "SIGTSTP";
            printf("Regardez du côté du SIGUSR\n");
            break;

        case SIGUSR1:
            signal_name = "SIGUSR1";
            printf("presque presque\n");
            break;
            
        case SIGUSR2:
            signal_name = "SIGUSR2";
            raise(SIGKILL);
        default:
            return;
    }
}
int main(void) {
    
    struct sigaction act;
    printf("My pid is: %d\n", getpid());

    act.sa_handler = &handle_signal;

    act.sa_flags = SA_RESTART;

    sigfillset(&act.sa_mask);

    if (sigaction(SIGINT, &act, NULL) == -1) {}
    if (sigaction(SIGTSTP, &act, NULL) == -1) {}
    if (sigaction(SIGUSR1, &act, NULL) == -1) {}
    if (sigaction(SIGUSR2, &act, NULL) == -1) {}
    while (keepRunning) {}
    
    return 0;
}