#include <stdlib.h> 
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

//Mini shell

static volatile int keepRunning = 1;
sigset_t x;

void tstpHandler(int dummy) {
    printf("deblocage du signal par CTRC + Z \n");
    sigprocmask(SIG_UNBLOCK, &x, NULL);
}

int main(void) {
    sigemptyset (&x);
    sigaddset(&x, SIGINT);
    sigprocmask(SIG_SETMASK, &x, NULL);
    signal(SIGTSTP,tstpHandler);
    
    while (keepRunning) {}
    return 0;
}
