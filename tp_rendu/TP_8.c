#include <stdlib.h> 
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

//Mini shell

static volatile int keepRunning=1;
int count=0;

void intHandler(int dummy) {
    printf("tester CTRL+Z\n");
    count +=5;
}

void tstpHandler(int dummy) {
    printf("Regardez du côté du SIGUSR\n");
    count +=10;
}

void usrpHandler(int dummy) {
    printf("presque presque\n");
    count +=1;
    
}

void exitHandler(int dummy) {
    printf("%d \n",count);
    raise(SIGKILL);
}

int main(void) {
    
    signal(SIGINT, intHandler); 
    signal(SIGTSTP, tstpHandler);
    signal(SIGUSR1, usrpHandler);
    signal(SIGUSR2, exitHandler);
    while (keepRunning) {}

    return 0;
}
