#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>



/**
* socket, bind, listen, accept, ACCEPT CONNECTION, read, DATA_PROCESS, wirte, close
*
*/

#define PORT 8080 
struct sockaddr_in server, client;

int main(int argc, char *argv[])
{
    int socket_fd = 0, accept_fd = 0, response, new_socket;
    socklen_t clientSize = 0;
    char *msg = "__server - hello i'm the server";
    char buffer[1024] = {0};

    server.sin_family = AF_INET;
    inet_pton(AF_INET, "127.0.0.1", &server.sin_addr);
    server.sin_port = htons(PORT);

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    bind(socket_fd, (struct sockaddr*)&server, sizeof(server));
    listen(socket_fd, 10);

    clientSize = sizeof(client);
    if ((new_socket = accept(socket_fd, (struct sockaddr *)&client, (socklen_t*)&clientSize))<0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    response = read(new_socket, buffer, 1024);
    printf("%s\n", buffer);

    write(new_socket, msg, sizeof(msg)) != sizeof(msg);
    printf("Hello message sent\n");
    return 0;

}